import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe:any = {};
  @Input() index:number;

  @Output() seleccionarHeroe: EventEmitter<number>;

  private router:Router;

  constructor(router:Router) {
    this.router = router;
    this.seleccionarHeroe = new EventEmitter();
  }

  ngOnInit() {
  }

  public getHeroe(){
    // console.log(this.index);
    this.router.navigate(['/heroe', this.index]);
    // this.seleccionarHeroe.emit( this.index );
  }

}
