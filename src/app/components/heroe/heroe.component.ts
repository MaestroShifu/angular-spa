import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  private activatedRoute:ActivatedRoute;
  private _heroeService:HeroesService;

  private heroe:Heroe;

  constructor(activatedRoute:ActivatedRoute, _heroeService:HeroesService) { 
    this._heroeService = _heroeService;
    this.activatedRoute = activatedRoute;

    this.activatedRoute.params.subscribe(params=>{
      this.heroe = this._heroeService.getHeroe(params['idHeroe']);
    }); 
  }

  ngOnInit() {
  }

}
