import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  private activatedRoute:ActivatedRoute;
  private _heroeService:HeroesService;

  private heroes:Heroe[];
  private key:string;

  constructor(activatedRoute:ActivatedRoute, _heroeService:HeroesService) {
    this._heroeService = _heroeService;
    this.activatedRoute = activatedRoute; 

    this.activatedRoute.params.subscribe(params=>{
      this.key = params['key'];
      this.heroes = this._heroeService.buscarHeroes(this.key);
    }); 
  }

  ngOnInit() {
  }

}
