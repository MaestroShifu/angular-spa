import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  private heroes:Heroe[];

  private _heroesService:HeroesService;
  private router:Router;

  constructor(_heroesService:HeroesService, router:Router) { 
    this._heroesService = _heroesService;
    this.router = router;
  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
  }

  public getHeroe(index:number) {
    this.router.navigate(['/heroe', index]);
  }

}
